install-local:
	ansible-galaxy install -r requirements.yaml

backup-home:
	ansible-playbook --extra-vars="@./group_vars/home.yaml" backup.yaml
backup-workstation:
	ansible-playbook --extra-vars="@./group_vars/laptop.yaml" backup.yaml

